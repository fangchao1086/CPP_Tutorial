#include <iostream>
#include <ctime>

using namespace std;

int main()
{
    clock_t begin, end;
    begin = clock();
    for (size_t i = 0; i < 1000000; i++)
    {
        i++;
    }
    end = clock();
    std::cout << "CLOCKS_PER_SEC: " << CLOCKS_PER_SEC << endl;
    std::cout << "运行时间: " << double(end - begin) / CLOCKS_PER_SEC << "s" << endl;
     std::cout << "运行时间: " << double(end - begin) << "ms" << endl;
}

/*
CLOCKS_PER_SEC: 1000
运行时间: 0.002s
运行时间: 2ms
*/