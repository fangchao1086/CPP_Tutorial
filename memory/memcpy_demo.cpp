#include <stdlib.h>
#include <stdio.h>
#include <string>


/*
有一个20个字节的内存a,
从a中拷贝最后5个字节到新的内存中
*/
int main() {
    char * a = (char *)malloc(20);
    char * b = (char *)malloc(5);
    memcpy(b, a + 15, 5);
    free(a);
    free(b);
    return 0;
}