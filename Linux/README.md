# 定时功能
查看服务是否开启：service cron status

启动服务：service cron start

关闭服务：service cron stop

重启服务：service cron restart

重新载入配置：service cron reload


添加定时任务
crontab -e

exp：下面两个任务表示每分钟都执行一次。前5个\*分别表示分钟，小时，月，年，周

每10分钟执行一次\*/10

\* \* \* \* \* echo "fgadsjklh" >> /mnt/e/Projects/Ubuntu/xiongdilian/xingqiwu.txt

\* \* \* \* \* /mnt/e/Projects/Ubuntu/xiongdilian/add_date_toxingqiwu.sh

查看定时任务
crontab -l

删除全部定时任务
crontab -r

ps: 对于linux用户而言，有可能普通用户无法开启服务，这时候需要登录root用户操作

# 文件下载
-O: 表示重命名
wget -O a.zip https://gitee.com/fangchao1086/CPP_Tutorial/repository/archive/master.zip

# expect交互
安装：sudo apt install expect
#!/bin/expect

set timeout 30
spawn su root                                 # spawn是执行的命令
expect "Password:"                            # 命令行屏幕出现的内容
send "root\n"                                 # 输入的命令
expect "#"                                    # 屏幕出现的值
send "echo -e  'this is expect script'\n"     # 执行命令行echo内容
interact

