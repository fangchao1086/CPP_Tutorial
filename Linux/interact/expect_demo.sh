#!/bin/expect

set timeout 30
spawn su root                                  # spawn后面是输入的命令
expect "Password:"                             # 匹配屏幕上出现的内容
send "root\n"                                  # 在屏幕的命令行上输入
expect "#"                                     # 匹配屏幕上的内容
send "echo -e  'this is expect script'\n"      # 输入echo
interact
