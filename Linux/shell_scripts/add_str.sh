#!/bin/bash

# 向$2文件末尾添加$1行个字符串
echo "$0"
rows=$(cat 1.txt |wc -l)
file=$2 

for ((i=0;i<$1;i++))
	do
		echo "rows: $(($rows+i))"
		sed -i ''$(("$rows"+i))' a this is row: '$(("$rows"+ i))'' $file
	done
