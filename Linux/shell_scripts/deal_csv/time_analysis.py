import csv
import xlrd
import numpy as np

# save predict xlsx to csv
book = xlrd.open_workbook("./csv_xlsx/predict.xlsx")
names = book.sheet_names()
sheet = book.sheet_by_index(0)
rows = sheet.nrows
with open('./csv_xlsx/predict.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)
    for i in range(rows):
        row = sheet.row_values(i)
        writer.writerow(row)
    csvfile.close()
# save real xlsx to csv
book = xlrd.open_workbook("./csv_xlsx/real.xlsx")
names = book.sheet_names()
sheet = book.sheet_by_index(0)
rows = sheet.nrows
with open('./csv_xlsx/real.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)
    for i in range(rows):
        row = sheet.row_values(i)
        writer.writerow(row)
    csvfile.close()

with open("./csv_xlsx/predict.csv") as f:
    reader = csv.reader(f)
    rows_predict = [row for row in reader]
    print("predict data:\n", rows_predict)



with open("./csv_xlsx/real.csv") as f:
    reader = csv.reader(f)
    rows_real = [row for row in reader]
    print("real data:\n", rows_real)

if(len(rows_predict) != len(rows_real)):
    print("wrong data")
else:
    # 统计预测值
    for i in range(len(rows_predict)):
        if(i==0):
            rows_predict[i].append(str(float(rows_predict[i][1])-float(rows_predict[i][0])))
        else:
            rows_predict[i].append(str(float(rows_predict[i][1])-float(rows_predict[i-1][1])))
    print("predict: ", rows_predict)

    # 统计真实值
    for i in range(len(rows_real)):
        if(i==0):
            rows_real[i].append(str(float(rows_real[i][1])-float(rows_real[i][0])))
        else:
            rows_real[i].append(str(float(rows_real[i][1])-float(rows_real[i-1][1])))
    print("real: ", rows_real)

    rate = [0 for i in range(len(rows_real))]
    print("rate: ", rate)
    for i in range(len(rate)):
        rate[i] = float(float(rows_real[i][2])/float(rows_predict[i][2]))
    print("rate: ", rate)
#     获取排序rate后的索引
    index = np.argsort(rate)
    # index = [i+1 for i in index]
    print(index) # 正序输出索引
    print(list(reversed(index))) # 逆序输出索引

    with open("./csv_xlsx/rate.csv",'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        for i in range(len(rate)):
            writer.writerow([rate[i]])
        csvfile.close()