#!/bin/bash

# 存数据
cat s_split.csv|cut -d ',' -f 1,2,3|grep  '_0' > s_0.csv
cat s_split.csv|cut -d ',' -f 1,2,3|grep  '_1' > s_1.csv

# 合并数据
paste -d ',' s_0.csv s_1.csv| awk -F ',' '{print $2","$4","$2+$4}' > get_value_0_1.csv

# 合并原始数据与新数据
paste -d ',' s.csv get_value_0_1.csv| awk -F ',' '{OFS=","}{print $1,$2,$3,$4,$5,$5-$2}' > s_concat.csv

sed -i '1i key,value,value_0,value_1,sum_0_1,sum_0_1-value' s_concat.csv

# 删除过程文件
rm -f s_0.csv s_1.csv get_value_0_1.csv

# 判断split合并后与原始文件的差值
awk -F ',' '{if(5<$6) print $1}' s_concat.csv > small_value.csv

# 打印内容i
echo -e "big string:"
sed -i '1d' small_value.csv
for line in `cat small_value.csv`
do
	echo "$line"
done
