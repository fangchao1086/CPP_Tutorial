import numpy as np
import pandas as pd
import csv


with open("s_concat.csv") as f:
    reader = csv.reader(f)
    rows=[row for row in reader]
    print("rows\n",rows)
    before = 0
    len_ = len(rows[0])
    for i in range(len(rows)):
        if i == 0:
            rows[i].append("row1-row0")
            continue
        now = rows[i][len_-1]
        print("now : ",now,", before", before)
        rows[i].append(str(int(now)-int(before)))
        before = rows[i][len_-1]

    for i in rows:
        print("cur row: ", i)

    # save
    with open('n_new_py.csv', 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        for i in rows:
            writer.writerow(i)
        csvfile.close();


